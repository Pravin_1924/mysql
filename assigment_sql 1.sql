create database Assigment;
create database Assignment;
 use assignment;
 ------------------------------------------------------------------------
 create table  Countries(
 roll_no int primary key auto_increment,
 contries_name varchar(40),
 population int,
 capital varchar(50)default'-'
 );
 
 insert into countries
 values
 ( 1,'china',1382,'Beijing'),
 (2,'india',1326,'delhi'),
 (3,'united states',324,'washington d.c'),
 (4,'indonesia',260,'jakarta'),
 (5,'brazil' ,209 ,'brasilia'),
 (6,'pakistan',193 ,'islamabad'),
 (7,'nigeria',187 ,'abuja'),
 (8,'bangladesh',163 ,'dhaka'),
 (9,'russia',143,'mascow'),
 (10,'mexico',128 ,'mexico city'),
 (11,'japan',126 ,'tokya'),
 (12,'philiippines',102,'manila'),
 (13,'ethiopia',101,'addis ababa'),
 (14,'vietnam',94,'hanai'),
 (15,'egypt',93,'cario'),
 (16,'germany',81,'berlin'),
 (17,'iran',80,'teran'),
 (18,'turkey',79,'ankara'),
 (19,'congo',79,'kinshasa'),
 (20,'vietnam',94,'hanai'),
 (22,'italy',60,'rome'),
 (23,'south africa',53,'pretoria'),
 (24,'myanmar',54,'naypyidaw');
---------------------------------------------------------------------
select* from countries;
---------------------------------------------------------------------------------
insert into countries
 values (25,'sri lanka',20,'colombo'),
         (26,'australia',25,'canbrra');

-----------------------------------------------------------------------------
update countries 
set capital='new delhi'
where roll_no=2;
--------------------------------------------------------------------------------------


ALTER TABLE countries RENAME BIG_COUNTRIES;
----------------------------------------------------------------------------------------
create table suppliers(
supplier_id int primary key auto_increment,
supplier_name varchar(50),
location varchar(50));

  ------------------------------------------------------------------------------------------
 create table product(
 product_id int primary key auto_increment,
 product_name varchar(59) unique not null,
 supplier_id int ,
 foreign key(supplier_id) references suppliers(supplier_id));
 
 -----------------------------------------------------------------------------------------
 
 create table stock(
 id int primary key auto_increment,
 product_id int,
foreign key(product_id) references product(product_id),
balance_stock int);
 ----------------------------------------------------------------------------------------
 insert into suppliers
 values(1,'anil','pune'),
 (2,'nilesh','mumbai'),
 (3,'anil','kolkata');
 
 insert into product
 values(11,'pen','1'),
 (12,'notebook','2'),
 (13,'book','3');

insert into stock
values(20,'100'),
(21,'200'),
(23,'300');
--------------------------------------------------------------------------
alter table suppliers
modify column supplier_name varchar(59);
----------------------------------------------------------------------
drop table if exists emp;
CREATE TABLE  emp  (
   emp_id  int(11) NOT NULL,
   birth_date  date NOT NULL,
   first_name  varchar(14) NOT NULL,
   last_name  varchar(16) NOT NULL,
   gender  enum('M','F') NOT NULL,
   hire_date  date NOT NULL,
   salary  float(8,2) DEFAULT 7850.00
) ;

Alter table emp 
Add deptno varchar(40);
Update emp 
Set deptno='20'
Where emp_id%2;

Update emp 
Set deptno='30'
Where emp_id%3;

Update emp 
Set deptno='40'
Where emp_id%4;

Update emp 
Set deptno='50'
Where emp_id%5;

Update emp 
Set deptno='10'
Where emp_id%1 OR emp_id%6 OR emp_id%7 OR emp_id%8 OR emp_id% 9;

--------------------------------------------------------------------------
CREATE UNIQUE INDEX EMP_ID using hash on emp(emp_id);
      
 ---------------------------------------------------------------------------------