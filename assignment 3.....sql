create database assignment3;
use assignment3;
---------------------------------------------------------------------------------------
use assignment3;
-----------------------------------------------------------------------------------
create table customer(
customer_number int,
customer_name varchar(50),
purches_status varchar(50));

insert into customer
values(3001,'suchita',204.5),
       (3002,'ramesh',210),
       (3003,'ravi',101),
       (3005,'amit',250),
       (3006,'anil',256),
       (3007,'rahul',247),
       (3008,'uttam',895),
       (3009,'kedar',546),
       (3010,'nilesh',751);
       
       select customer_name,purches_status
       from customer
       order by customer_number;
 --------------------------------------------------------------g------------
 
create table employees(
emp_id int primary key not null ,
frist_name varchar(50),
last_name varchar(50),
location varchar(50),
salary int);

insert into employees
values(1,'satish','patil','kolhapur',50000),
	  (2,'pooja','deshmukh','pune',78000),
      (3,'suchita','surve','mumbai',80000),
      (4,'nitin','satpute','odisha',85000),
      (5,'amit','pawar','delhi',90000),
      (6,'amol','kumar','hydrabad',79850);
      
      SELECT SALARY 
      FROM EMPLOYEES a
      WHERE 3 >= (SELECT COUNT( SALARY)
      FROM EMPLOYEES b
      where b.salary >=a.salary)
      order by a.salary desc;
      
      -----------------------------------------------------------------------------------------
	
      SELECT
      FRIST_NAME,
      LAST_NAME,
      LOCATION,
      SALARY,
      RANK()OVER( ORDER BY SALARY DESC) AS SALARY_RANK
      FROM EMPLOYEES;
      
	
      WITH EMPLOYEES AS (
      SELECT SALARY,
      RANK() OVER (ORDER BY SALARY DESC) AS RANK_SALARY
      FROM EMPLOYEES)
       SELECT SALARY FROM EMPLOYEES 
       WHERE RANK_SALARY = 1 ;
--------------------------------------------------------------------------------
      
 
 DELIMITER //
 create procedure status_order(IN order_month varchar(50) , IN order_year varchar(50))
 BEGIN
  Select orderdate , status from orders where month(orderdate) = month(str_to_date(order_month , '%b')) and year(orderdate) = order_year ;
 END //
 
call status_order('Nov',2005);
----------------------------------------------------------------------------------------------------------------

CREATE TABLE cancellations (
    id INT AUTO_INCREMENT PRIMARY KEY,
    comments TEXT,
    ordernumber INT,
    customernumber INT,
    FOREIGN KEY (customernumber)
        REFERENCES customers (customernumber),
    FOREIGN KEY (ordernumber)
        REFERENCES orders (ordernumber)
); 

DELIMITER //
create procedure orders_cancelled()
BEGIN
insert into cancellations (ordernumber, customernumber ,comments) select ordernumber , customernumber , status from orders where status='cancelled';
END //

CALL orders_cancelled();

select * from cancellations;
--------------------------------------------------------------------------------------------------------------------------------
/* 3.


DELIMITER $$
CREATE FUNCTION purchase_status(amount DECIMAL(10,2)) 
RETURNS VARCHAR(20)
DETERMINISTIC
BEGIN
	DECLARE purchase_status VARCHAR(20);
    
    IF amount > 50000 THEN
		SET purchase_status = 'PLATINUM';
        
    ELSEIF (amount >= 25000 AND amount < 50000) THEN
        SET purchase_status = 'GOLD';
        
    ELSEIF amount < 25000 THEN
        SET purchase_status = 'SILVER';
        
    END IF;
	
	RETURN (purchase_status);
END$$

alter table payments add foreign key (customernumber) references customers(customernumber);

desc payments;

select p.customernumber, customername , 
(CASE
        WHEN amount < 25000 THEN 'Silver'
        WHEN amount <= 50000 THEN 'Gold'
        WHEN amount > 50000 THEN 'Platinum'
    END)AS purchase_status from payments as p inner join customers as c on p.customernumber=c.customernumber;
 ---------------------------------------------------------------------------------------------------------------------------   

DELIMITER $$

CREATE TRIGGER after_movies_update
AFTER UPDATE
ON movies FOR EACH ROW
BEGIN
IF OLD.id <> new.id THEN
        INSERT INTO rentals(memid,first_name, last_name, movieid)
        VALUES(old.id, new.id);
    END IF;
    END$$
    
    DROP TRIGGER after_movies_update;
    
update rentals 
set movieid = 21
where memid = 6;

update rentals 
set movieid = 23
where memid = 7;

select * from rentals;


DELIMITER //
CREATE TRIGGER after_movies_delete
AFTER DELETE
ON movies FOR EACH ROW
BEGIN
delete from rentals
where movieid = old.id;
END //

delete from rentals where movieid= 1;

DROP TRIGGER after_movies_delete;


UPDATE rentals
SET movieid = old.id

---------------------------------------------------------------------------------------------------------------




